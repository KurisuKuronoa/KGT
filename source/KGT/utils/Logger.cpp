#include "Logger.h"

//TODO: CREATE A BETTER IMPLEMENTATION OF THIS
namespace Log {

	void debug(std::string message) {
		if (LEVEL_DEBUG <= logLevel)
			std::cout << mDebug << message << std::endl;
	}

	void info(std::string message){
		if(LEVEL_INFO <= Log::logLevel)
			printf((mInfo + message + "\n").c_str());
	}

	void warn(std::string message){
		if (LEVEL_WARN <= logLevel)
			printf((mWarn + message + "\n").c_str());
	}

	void error(std::string message){
		if (LEVEL_ERROR <= logLevel)
			printf((mError + message + "\n").c_str());
	}


	void setLogLevel(int logLevel){
		Log::logLevel = logLevel;
	}

}
