#include  "Thread.h"

namespace KGT {

	int Thread::numThreads = 0;

	s32 Thread::getMainThreadPriority() {
		s32 tmpPriority = 0;
		//FIXME: Allow function to operate outside of the main thread.
		svcGetThreadPriority(&tmpPriority, CUR_THREAD_HANDLE);
		return tmpPriority;
	}

	void Thread::join(u64 timeout) {
		threadJoin(this->thread, timeout);
	}
}
