#ifndef CANVAS_H
#define CANVAS_H

#include <3ds.h>
#include <vector>
#include "../math/Transformation.h"
#include "../math/TransformationStack.h"
#include "Image.h"
#include "Canvas.h"
#include "math.h"

#define Z_MUL z * depthOffset * depthSideMul
#define CONFIG_3D_SLIDERSTATE (*(float *)0x1FF81080)
extern "C" {
    #include <sf2d.h>
}

namespace KGT {
    class Canvas {
        friend class UI;
        public:
            Canvas(gfxScreen_t screen){
                this->screen = screen;
            }

            static const int TOP_WIDTH = 400;
            static const int TOP_HEIGHT = 240;
            static const int BOTTOM_WIDTH = 320;
            static const int BOTTOM_HEIGHT = 240;

			/**
			 * Enables the canvas to draw things
			 */
			void start(gfx3dSide_t side);
			void stop();

			
			void enableClipping(int x, int y, int width, int height);
			void disableClipping();

            /**
             * Draws a line. Will apply the transformation if enabled
             * \param x1 The x of the first point
             * \param y1 The y of the first point
             * \param x2 The x of the second point
			 * \param y2 The y of the second point
             */
            void drawLine(int x1, int y1, int x2, int y2, int z);
			void drawImage(Image* image, int x, int y, int z, bool blend);

            /* --------------------------------------
             * GETTERS AND SETTERS
             * -------------------------------------- */
            void setColor(u32 newColor){
                color = newColor;
            }

            void setColor(u8 r, u8 g, u8 b, u8 a){
                color = RGBA8(r,g,b,a);
            }

            u32 getColor(){
                return color;
            }

            void setLineWidth(int newWidth){
                lineWidth = newWidth;
            }

            int getLineWidth(){
                return lineWidth;
            }

            void setTransformationEnabled(bool enabled){
				transformEnabled = enabled;
            }

            bool isTransformationEnabled() {
				return transformEnabled;
            }

			void pushTransformation() {
				Transformation transformation = Transformation();
				transformationStack.push(transformation);
			}

            void pushTransformation(Transformation newTransformation) {
				transformationStack.push(newTransformation);
            }

			void popTransformation(){
				transformationStack.pop();
			}
			
			void clearTransformation(){
                transformationStack.reset();
            }

			Transformation* getTransformation() { return transformationStack.back(); }

            /**
             * Resets the color and line width of the canvas;
             */
			void reset() {
				color = RGBA8(1, 1, 1, 1);
				lineWidth = 1;
			}

        protected:
            /**
             * The amount of
             */
			float depthOffset = 2.3 ;
			float depthSideMul = 0.0;
			/**
			 * If KGT should enable 3D rendering.
			 */
			bool threeD = false;
			/**
			 * If the canvas applies transforms. Keep disabled if you don't use it.
			 */
			bool transformEnabled = false;
			/**
			 * The transformation of the canvas
			 */
			TransformationStack transformationStack;

			gfx3dSide_t currentSide;
            u32 color = RGBA8(1, 1, 1, 1);
            gfxScreen_t screen;
            float depth = 1.0;
            int lineWidth = 1;
            bool isDrawing = false;
    };

}

#endif // CANVAS_H
