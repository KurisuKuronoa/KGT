#ifndef H_WIDGET
#define H_WIDGET

#include <vector>
#include "Action.h"
#include "../../math/Transformation.h"
#include "../Canvas.h"

namespace KGT {
	class Action;
	class Widget {
		public:
			virtual void draw(Canvas* canvas);
			virtual void update(float delta);
			/**
			 * Called after the size of the Widget has been changed
			 */
			virtual void onSizeChanged();

			void applyTransformation(Canvas* canvas);
			void removeTransformation(Canvas* canvas);

			void addAction(Action action);

            float getWidth() {
                return width;
            }
            void setWidth(float newWidth) {
                width = newWidth;
				onSizeChanged();
            }

            float getHeight() {
                return height;
            }

            void setHeight(float newHeight) {
                height = newHeight;
				onSizeChanged();
            }

			int getX() {
				return x;
			}

            void setX(int newX) {
				x = newX;
            }

			int getY() {
				return y;
			}

            void setY(int newY) {
				y = newY;
            }

            void setZ(int newZ) {
				z = newZ;
            }

            int getZ() {
				return z;
            }

            void setTransformation(Transformation transformation) {
				this->transformation = transformation;
            }

            Transformation* getTransformation(){
				return &transformation;
            }

        protected:
            int x, y, z;
            int width, height;
            std::vector<Action> actions;

            int preferredHeight, preferredWidth;
            Transformation transformation;
    };
}
#endif // H_WIDGET
