#ifndef CONTAINER_H
#define CONTAINER_H
#include <vector>
#include "Widget.h"
#include "../../utils/Logger.h"

namespace KGT{
    enum Align {
        TOP = 0x01,
        LEFT = 0x02,
        RIGHT = 0x04,
        BOTTOM = 0x08,
        CENTER = TOP | LEFT | RIGHT | BOTTOM,
        CENTER_H = LEFT | RIGHT,
        CENTER_V = TOP | BOTTOM
    };
    class Container : public Widget{
        public:
			Container() {
				marginLeft = marginTop = marginRight = marginBottom = 0;
			}
			virtual void draw(Canvas* canvas);
            void invalidate();
            void addChild(Widget* child){
                children.push_back(child);
                invalidate();
            }
            virtual void update(float delta);

            int getAlign(){
                return align;
            }

            void setAlign(int newAlign){
                align = newAlign;
                invalidate();
            }

            void setMargin(int margin){
                marginLeft = margin;
                marginBottom = margin;
                marginTop = margin;
                marginRight = margin;
                invalidate();
            }


        protected:
            bool isValidated = false;
            virtual void layout();
            void validate();
			int marginLeft, marginTop, marginRight, marginBottom;
            int align;
            std::vector<Widget*> children;
    };
}


#endif // CONTAINER_H
