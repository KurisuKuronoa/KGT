#ifndef ACTION_H
#define ACTION_H

#include "Widget.h"
#include "../../math/Interpolation.h"

namespace KGT{

	class Widget;
	
    class Action {
		friend Widget;
        public:
            Action(float duration, BaseInterpolation interpolation){
                this->duration = duration;
                this->interpolation = interpolation;
            }
            /**
            * Updates the action. Subclasses should call this too
            * \param progress Value between 0 and 1, where 0 is the beginning and 1 is the end.
            */
            virtual void update(float progress);

			void stop() { finished = true; }
            bool isFinished() { return finished; }

			Widget* getWidget() { return widget; }
        protected:
			Widget* widget;
			float delay;
            float duration;
            float timePassed;
			BaseInterpolation interpolation;
            bool finished = false;
        private:
			void internalUpdate(float delta);
			void setWidget(Widget* widget) {
				this->widget = widget;
			}
    };
}

#endif // ACTION_H
