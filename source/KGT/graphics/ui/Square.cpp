#include "Square.h"
#include "../../utils/Logger.h"

namespace KGT {

    void Square::draw(Canvas* canvas){
		canvas->reset();
		canvas->setLineWidth(10);
		int halfWidth = 10 / 2;
		canvas->setColor(0xFF, 0xFF, 0x0, 0xFF);

        canvas->drawLine(this->x + halfWidth, this->y + halfWidth,
						 this->x + this->width - halfWidth, this->y + halfWidth, this->z);

        canvas->drawLine(this->x + halfWidth, this->y + halfWidth,
						 this->x + halfWidth, this->y + this->height - halfWidth, this->z);

        canvas->drawLine(this->x + halfWidth, 				this->y + this->height - halfWidth,
						 this->x + this->width - halfWidth, this->y + this->height - halfWidth, this->z);

        canvas->drawLine(this->x + this->width - halfWidth, this->y + halfWidth,
						 this->x + this->width - halfWidth, this->y + this->height - halfWidth, this->z);
    };

    void Square::update(float delta){
		Widget::update(delta);
    }

}
