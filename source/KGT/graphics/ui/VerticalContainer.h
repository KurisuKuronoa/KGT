#ifndef VERTICALGROUP_H
#define VERTICALGROUP_H

#include "Container.h"
#include "../../utils/Logger.h"
#include <string>

namespace KGT{

    class VerticalContainer : public Container {
        public:
			virtual void update(float delta);
        protected:
            virtual void layout();
    };

}

#endif // VERTICALGROUP_H
