#include "Container.h"
#include <3ds.h>

namespace KGT{

	void Container::draw(Canvas* canvas) {
        for(auto &child : children) {
            canvas->pushTransformation((*child->getTransformation()));
			child->draw(canvas);
            canvas->popTransformation();
        }
	}

    void Container::invalidate() {
		Log::debug("Invalidated");
        isValidated = false;
    }

    void Container::layout() {
        for (auto &child : children) {
			child->setX(0);
			child->setY(0);
			child->setWidth(this->getWidth());
			child->setHeight(this->getHeight());
        }
        validate();
    }

    void Container::update(float delta) {
        if(!isValidated) {
            layout();
        }
        for (auto &i : children) {
            i->update(delta);
        }
    }

    void Container::validate() {
		isValidated = true;
    }
}
