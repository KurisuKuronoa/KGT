#ifndef SQUARE_H
#define SQUARE_H

#include "Widget.h"
#include "../../utils/Logger.h"
#include <string>

namespace KGT {

    class Square : public Widget {
        public:
			Square(){
				Log::debug("---[SQUARE]---");
				Log::debug(std::to_string(this->x));
			}
            virtual void draw(Canvas* canvas);
            virtual void update(float delta);
    };

}

#endif // SQUARE_H
