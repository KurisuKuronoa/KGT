#include "Widget.h"
#include <vector>

namespace KGT{
    void Widget::draw(Canvas* canvas){

    }

    void Widget::update(float delta){
		std::vector<int> pendingToBeRemoved;
		int i = 0;
		for(auto &action : actions) {
			action.internalUpdate(delta);
			if(action.isFinished()){
                pendingToBeRemoved.push_back(i);
			}
			i++;
		}
		int countErased = 0;
		for(auto &index : pendingToBeRemoved){
			actions.erase(actions.begin() + index - countErased);
		}
		pendingToBeRemoved.clear();
    }

	void Widget::onSizeChanged() {

	}

	void Widget::addAction(Action action) {
		this->actions.push_back(action);
	}

	void Widget::applyTransformation(Canvas* canvas) {
		//canvas->setTransformation();
		//this->getTransformation()->setPivotX(this->getTransformation()->getPivotX() - this->x);
		//this->getTransformation()->setPivotY(this->getTransformation()->getPivotY() - this->y);
		canvas->pushTransformation(this->transformation);
	}

	void Widget::removeTransformation(Canvas* canvas) {
		//canvas->setTransformation(canvas->getTransformation() - this->getTransformation());
		canvas->popTransformation();
		//this->getTransformation()->setPivotX(this->getTransformation()->getPivotX() + this->x);
		//this->getTransformation()->setPivotY(this->getTransformation()->getPivotY() + this->y);
	}
}
