#include "ImageWidget.h"

namespace KGT {

	void ImageWidget::draw(Canvas* canvas) {
		if (this->image) {
			//Log::debug("Enabling clipping");
			//canvas->enableClipping(this->x, this->y, this->width, this->height);
			//Log::debug("Drawing image");
			canvas->drawImage(this->image, this->x + calculatedX, this->y + calculatedY, this->z, false);
			//Log::debug("Disabling clipping");
			//canvas->disableClipping();
			//Log::debug("Image done");
		}
	}

	void ImageWidget::onSizeChanged() {
		switch(this->scaleType) {
			case ORIGINAL:
				calculatedX = this->getWidth() / 2 - this->getImage()->getWidth() / 2;
				calculatedY = this->getHeight() / 2 - this->getImage()->getHeight() / 2;
				calculatedScaleX = 1;
				calculatedScaleY = 1;
				break;
		}
	}

}