#ifndef DIVIDER_H
#define DIVIDER_H

#include "../Canvas.h"
#include "Widget.h"

namespace KGT {

    class Divider : public Widget {
		public:
			virtual void draw(Canvas* canvas);
    };
}

#endif //DIVIDER_H
