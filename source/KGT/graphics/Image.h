#ifndef IMAGE_H
#define IMAGE_H

#include <sf2d.h>
#include <sfil.h>
#include <string>

namespace KGT {

	enum ImageType {
		PNG,
		JPEG,
		BMP
	};

	class Image {
		public:
			Image(std::string path, ImageType imgType, sf2d_place place = SF2D_PLACE_RAM) {
				switch (imgType){
					case PNG:
						texture = sfil_load_PNG_file(path.c_str(), place);
						break;
					case JPEG:
						texture = sfil_load_JPEG_file(path.c_str(), place);
						break;
					case BMP:
						texture = sfil_load_BMP_file(path.c_str(), place);
				}
			}

			~Image() {
				sf2d_free_texture(texture);
			}

			/**
			 * \return A sf2d_texture to be used by SF2D
			 */
			sf2d_texture* getTexture() { return texture; }
			int getWidth() { return texture->width; }
			int getHeight() { return texture->height; }
			//int getDataSize() { return texture->data_size; }
		private:
			sf2d_texture* texture;
	};

}

#endif //IMAGE_H
