#include "TransformationStack.h"

namespace KGT {
	
	size_t TransformationStack::pop() {
		stack.pop_back();
		return stack.size();
	}

	size_t TransformationStack::push(Transformation transformation) {
		stack.push_back(transformation);
		return stack.size();
	}

	void TransformationStack::reset() {
		stack.clear();
        Transformation tmp = Transformation();
        this->push(tmp);
	}

	void TransformationStack::transform(int* x, int* y) {
		for (Transformation transform : stack) {
            transform.transform(x, y);
        }
	}

	void TransformationStack::transform(float* x, float* y) {
		for (Transformation transform : stack) {
            transform.transform(x, y);
        }
	}

}
