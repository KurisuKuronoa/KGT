#ifndef INTERPOLATION_H
#define INTERPOLATION_H

#include "math.h"
#define PI 3.1416926
namespace KGT {

	class BaseInterpolation {
		public:
			/**
			 * Applies the interpolation.
			 * \param value A value between 0 and 1
			 * \return The interpolated value between 0 and 1
			 */
			virtual float apply(float value);

			/**
			 * Convience method
			 */
			float apply(float start, float end, float value);

	};

	class Sine : public BaseInterpolation {
		public:
			virtual float apply(float value);
	};

	class Steps : public BaseInterpolation {
		public:
			Steps(int steps) {}
			virtual float apply(float value);
		private:
			int steps;
	};

	namespace Interpolation {
		const static Sine sine;
	}
}

#endif // INTERPOLATION_H
