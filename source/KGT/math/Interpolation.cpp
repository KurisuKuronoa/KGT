#include "Interpolation.h"

namespace KGT {

		float BaseInterpolation::apply(float start, float end, float value) {
			return start + (end - start) * apply(value);
		}

		float BaseInterpolation::apply(float value) {
			return value;
		}
	
		float Sine::apply(float value) {
			return (1 - std::cos(value * PI * 2)) / 2;
		}

		float Steps::apply(float value) {
			return std::floor(value * steps) / steps;
		}

}
