#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H

#include "math.h"
#include "../utils/Logger.h"
#include "string.h"

namespace KGT {

	class Transformation {

		public:
			void setRot(float rotation) {
				this->rotation = rotation;
			}

			void rotateBy(float rotation) {
				this->rotation += rotation;
			}

			float getRotation() {
				return rotation;
			}

            void setScale(float newScale) {
				scaleX = newScale;
				scaleY = newScale;
            }

            float getScaleX() {
				return scaleX;
            }

            float getScaleY() {
				return scaleY;
            }

            void scaleBy(float scale) {
				scaleX *= scale;
				scaleY *= scale;
            }

            void scaleBy(float x, float y){
				scaleX *= x;
				scaleY *= y;
            }

            void scaleByX(float x) {
				scaleX *= x;
            }

            void scaleByY(float y) {
				scaleY *= y;
            }

			void translateBy(float x, float y) {
                translateX += x;
                translateY += y;
			}

			void setTranslation(float x, float y) {
				translateX = x;
				translateY = y;
			}

			float getTransX() {
				return translateX;
			}

			float getTransY() {
				return translateY;
			}

			void setPivot(float x, float y) {
				pivotX = x;
				pivotY = y;
			}

			void setPivotX(float x) {
				this->pivotX = x;
			}

			void setPivotY(float y) {
				this->pivotY = y;
			}

			float getPivotX() { return pivotX; }
			float getPivotY() { return pivotY; }

			/**
			 * Transforms a point in 2D space
			 */
			void transform(float* x, float* y);

			/**
			 * Transforms a point in 2D space
			 */
			void transform(int* x, int* y);
            
            //Doing this was a mistake. Will be removed
			/*Transformation operator+=(const Transformation &other);
			Transformation operator-=(const Transformation &other);
			Transformation operator+(const Transformation &other);
			Transformation operator-(const Transformation &other);*/

		private:
			/**
			 * The rotation, in radians
			 */
			float rotation = 0;
			float translateX = 0;
			float translateY = 0;
			float scaleX = 1;
			float scaleY = 1;
			float pivotX = 0;
			float pivotY = 0;
	};

}


#endif // TRANSFORMATION_H
