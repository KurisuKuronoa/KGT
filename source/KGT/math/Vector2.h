#ifndef VECTOR2_H
#define VECTOR2_H

#include <math.h>

namespace KGT {
	
	class Vector2 {
		public:
			Vector2(float x = 0, float y = 0) {
				this->x = x;
				this->y = y;
			}

			Vector2(Vector2 &other) {
				this->x = other.x;
				this->y = other.y;
			}

			float distance(Vector2 &other);

			float getX() { return x; }
			float getY() { return y; }
			
		protected:
			float x, y;
	};
}

#endif //VECTOR2_H