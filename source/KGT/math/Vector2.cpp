#include "Vector2.h"

namespace KGT {

	float Vector2::distance(Vector2 &other) {
		return std::sqrt( std::pow(this->x + other.x, 2) + std::pow(this->y + other.y, 2) );
	}

}