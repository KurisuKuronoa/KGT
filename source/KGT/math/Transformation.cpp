#include "Transformation.h"

namespace KGT {

	void Transformation::transform(float* x, float* y){
		int oldX, oldY;
		*x -= pivotX;
		*y -= pivotY;
		oldX = *x;
		oldY = *y;
		*x = oldX * std::cos(rotation) - oldY * std::sin(rotation) + translateX;
		*y = oldX * std::sin(rotation) + oldY * std::cos(rotation) + translateY;
		*x += pivotX;
		*y += pivotY;
	}

	/**
	 * Transforms a point in 2D space
	 */
	void Transformation::transform(int* x, int* y){
		int oldX, oldY;
		*x -= pivotX;
		*y -= pivotY;
		oldX = *x;
		oldY = *y;
		*x = (oldX * std::cos(rotation) - oldY * std::sin(rotation)) * scaleX + translateX;
		*y = (oldX * std::sin(rotation) + oldY * std::cos(rotation)) * scaleY + translateY;
		*x += pivotX;
		*y += pivotY;
	}

	/*Transformation Transformation::operator+=(const Transformation& transformation) {
        this->scaleX *= transformation.scaleX;
        this->scaleY *= transformation.scaleY;
        this->rotation += transformation.rotation;
        this->pivotX += transformation.pivotX;
        this->pivotY += transformation.pivotY;
        this->translateX += transformation.translateX;
        this->translateY += transformation.translateY;
        return *this;
	}

	Transformation Transformation::operator-=(const Transformation& transformation) {
        this->scaleX *= 1 / transformation.scaleX;
        this->scaleY *= 1 / transformation.scaleY;
        this->rotation -= transformation.rotation;
        this->pivotX -= transformation.pivotX;
        this->pivotY -= transformation.pivotY;
        this->translateX -= transformation.translateX;
        this->translateY -= transformation.translateY;
        return *this;
	}

	Transformation Transformation::operator+(const Transformation& other) {
		Transformation transformation = *this;
		transformation += other;
		return transformation;
	}

	Transformation Transformation::operator-(const Transformation& other) {
		Transformation transformation = *this;
		transformation -= other;
		return transformation;
	}*/

}
