/**
 * Copied file from TWLoader on 26 februari 2017. Source file: gui/source/sound.h
 */

#pragma once
#include <3ds.h>
#include <string>

class WavSound {
    public:
        WavSound(const std::string& path, int channel = 1, bool toloop = true);
        ~WavSound();
        void play();
        void stop();
    private:
        u32 dataSize;
        ndspWaveBuf waveBuf;
        u8* data = NULL;
        int chnl;
};
