#include <3ds.h>
#include <3ds/types.h>
#include <cstring>
#include <string>
#include <stdio.h>
#include <cstdio>
#include <unistd.h>
#include "KGT/KGT.h"


extern "C"{
    #include <sf2d.h>
}
#define CONFIG_3D_SLIDERSTATE (*(float *)0x1FF81080)

bool supportsSound();
bool initGraphics();
const float DELTA = 1/30;
bool showConsole = true;
void busyThread(void* arg);

int main(int argc, char **argv) {
    aptInit();

    //In case the graphics fail to initialize
    if(!initGraphics()) {
		return -1;
    }
    romfsInit();
    consoleInit(GFX_BOTTOM, NULL);

    if(supportsSound()){
        Log::info("Sound supported!");
        ndspInit();
    }

    Log::debug("Dong ding start!");



    sf2d_set_clear_color(RGBA8(0x00, 0x00, 0x00, 0xFF));
    Log::debug("Clear color set");
    KGT::UI mUI = KGT::UI();
    Log::debug("UI inited");
    KGT::VerticalContainer root = KGT::VerticalContainer();
    Log::debug("VerticalContainer created");
    KGT::Square test = KGT::Square();
    test.setWidth(100);
    test.setHeight(60);
    test.setZ(1);


	KGT::Image image = KGT::Image("romfs:/ui/loading.png", KGT::PNG);
	KGT::ImageWidget imageWidget = KGT::ImageWidget(&image);
	imageWidget.setWidth(50);
	imageWidget.setHeight(50);

    KGT::Square test2 = KGT::Square();
    test2.setWidth(100);
    test2.setHeight(60);
    test2.setZ(3);

    KGT::Square test3 = KGT::Square();
    test3.setWidth(120);
    test3.setHeight(80);
    test3.setZ(7);

    root.addChild(&test);
	root.addChild(&imageWidget);
    root.addChild(&test2);
    root.addChild(&test3);
    root.setMargin(20);
	
    mUI.setTopContainer(&root);
    //UI.setBottomContainer(&root);
    mUI.set3DEnabled(true);

    Log::info("Start!");
    if(NULL){
		Log::error("Should return false!");
    }else{
		Log::warn("All good, no warning required!");
    }
    WavSound mSound = WavSound("romfs:/loading.wav");
    mSound.play();
    while(aptMainLoop()) {
		//REMOVEME:
		//root.getTransformation()->rotateBy(0.012);
		//imageWidget.getTransformation()->rotateBy(0.015);
		
        hidScanInput();
        mUI.draw();
        mUI.update(DELTA);
        sf2d_swapbuffers();
        if(hidKeysUp() & KEY_START){
            break;
        }
        if(hidKeysUp() & KEY_L) {
			showConsole = !showConsole;
			if(showConsole) {
				mUI.setBottomContainer(NULL);
			}else{
				mUI.setBottomContainer(&root);
			}
        }
    }
    mSound.stop();
    ndspExit();
    romfsExit();
    sf2d_fini();
    Log::info("Quitting...");
    aptExit();
    //REMINDER: IF YOUR HOMEBREW DOESN'T RETURN TO THE LAUNCHER, CHECK IF EVERY INITIALIZED SERVICE IS EXITED!
    return 0;
}

bool supportsSound(){
    return access( "sdmc:/3ds/dspfirm.cdc", F_OK ) != -1;
}

bool initGraphics() {
	if(!sf2d_init()){
		consoleInit(GFX_BOTTOM, NULL);
        Log::error("Failed to initialize graphics");
        Log::error("Press start or home to exit");

        for(int i = 0; i < 15; i++)
			Log::info("");

        Log::info("    ,--.");
        Log::info("   ([ oo]");
        Log::info("    `- ^\\");
        Log::info("  _  I`-'");
        Log::info(",o(`-V'");
        Log::info("|( `-H-'");
        Log::info("|(`--A-'");
        Log::info("|(`-/_\\'\\");
        Log::info("O `'I ``\\\\");
        Log::info("(\\  I    |\\,");
        Log::info(" \\\\-T-\"`, |H  Ojo");
        Log::info("Doot doot");

        while(aptMainLoop()){
            hidScanInput();
            if(hidKeysDown() & KEY_START){
                break;
            }
        }
        sf2d_fini();
        aptExit();
        return false;
    }else {
		return true;
    }
}

void busyThread(void* arg) {
    for(int i = 0; i < 30; i++){
		Log::debug(std::to_string(i));
		svcSleepThread(1000000 * 1000);
    }
    Log::debug("Thread done");
}
